import java.sql.*;

public class jdbcdemo {
    public static void main(String[] args) {
        System.out.println("JDBC Demo ");
        String name = "Hallo Hans";
        String email = "Hallo@Hans.at";
        int id = 2;

        selectAllDemo();
        insertDemo(name, email);
        selectAllDemo();
        updateDemo(name, email, id);
        deleteDemo(id);
        selectAllDemo();
        findAllByName("Hans");
        findAllByEmail("Hans");

        Teil2 teil2 = new Teil2(3);
        
    }



    private static void findAllByEmail(String emailSuche) {
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM student WHERE student.name like ?;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt fuer Fuer Email");
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            preparedStatement.setString(1, "%"+emailSuche+"%");
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int idAbfrage = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: mit der ID " + idAbfrage + " name: "+  name + " email: " + email);

            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }

    private static void findAllByName(String namesuche) {
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM student WHERE student.name like ?;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt fuer Fuer Name");
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            preparedStatement.setString(1, "%"+namesuche+"%");
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int idAbfrage = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: mit der ID " + idAbfrage + " name: "+  name + " email: " + email);

            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }


    public static void deleteDemo( int id){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "DELETE FROM student WHERE student.id = ?; ";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                System.out.println("Update Statement:   ");
                preparedStatement.setInt(1, id);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile(n) geloescht");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Insert Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }
    }
    public static void updateDemo(String neuerName, String neueEmail, int id){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "UPDATE student SET student.name = ?, student.email = ? WHERE student.id = ?";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                System.out.println("Update Statement:   ");
                preparedStatement.setString(1, neuerName);
                preparedStatement.setString(2, neueEmail);
                preparedStatement.setInt(3, id);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile Updated");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Insert Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }

    }
    public static void insertDemo(String name, String email){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "INSERT INTO student VALUES (NULL, ?, ?)";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile eingefuegt");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Insert Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }
    }

    public static void selectAllDemo(){
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM student;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: mit der ID " + id + " name: "+  name + " email: " + email);

            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }
}
