import java.sql.*;

public class Teil2 {
    private String strasse = "Kaisersweg";
    private int plz = 6372;
    private String ort = "Oberndorf";
    private int id = 0;
    public Teil2(int id){
        this.id = id;
        selectAllDemo();
        insertDemo(this.id, this.ort, this.strasse, this.plz);
        selectAllDemo();
        //deleteDemo(3);
        //selectAllDemo();
        findAllByOrt(this.ort);
        updateDemo(3,"Roemerweg", this.ort, this.plz);
        findAllByplz(this.plz);


    }

    private void findAllByplz( int plzSuche) {
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM adresse WHERE adresse.plz like ?;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt fuer plz");
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            preparedStatement.setString(1, "%"+plzSuche+"%");
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int idAbfrage = rs.getInt("id");
                String ort = rs.getString("ort");
                String strasse = rs.getString("strasse");
                int plzAbfrage = rs.getInt("id");
                System.out.println("Adresse aus der DB: mit der ID " + idAbfrage + " ort: "+  ort + " strasse: " + strasse + " Postleitzahl: " + plz);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }

    private void findAllByOrt(String namesuche) {
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM adresse WHERE adresse.ort like ?;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt fuer Ort " + namesuche);
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            preparedStatement.setString(1, "%"+namesuche+"%");
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int idAbfrage = rs.getInt("id");
                String ort = rs.getString("ort");
                String strasse = rs.getString("strasse");
                int plzAbfrage = rs.getInt("id");
                System.out.println("Adresse aus der DB: mit der ID " + idAbfrage + " ort: "+  ort + " strasse: " + strasse + " Postleitzahl: " + plz);

            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }


    public void deleteDemo( int id){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "DELETE FROM adresse WHERE adresse.id = ?; ";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                System.out.println("Update Statement:   ");
                preparedStatement.setInt(1, id);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile(n) geloescht");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Insert Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }
    }
    public void updateDemo(int id, String neueStrasse, String neuerOrt, int neuePLz){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "UPDATE adresse SET adresse.ort = ?, adresse.strasse = ?, adresse.plz =? WHERE adresse.id = ?";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                System.out.println("Update Statement:   ");
                preparedStatement.setString(1, neuerOrt);
                preparedStatement.setString(2, neueStrasse);
                preparedStatement.setInt(3, neuePLz);
                preparedStatement.setInt(4, id);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile Updated");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Update Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }

    }
    public void insertDemo(int id, String ort, String strasse, int plz){
        System.out.println("Select Demo mit JDBC");
        String sqlInsertRow = "INSERT INTO adresse VALUES (?, ?, ?,?)";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlInsertRow);
            try{
                preparedStatement.setInt(1, id);
                preparedStatement.setString(2, strasse);
                preparedStatement.setString(3,ort );
                preparedStatement.setInt(4, plz);
                int rowAffected =  preparedStatement.executeUpdate();
                System.out.println( rowAffected + "Zeile eingefuegt");

            }
            catch (SQLException ex){
                System.out.println("Fehler im SQL Insert Statement: "+  ex);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);

        }
    }

    public void selectAllDemo(){
        System.out.println("Select Demo mit JDBC");
        String sqlSelectAllPersonens = "SELECT * FROM adresse;";
        String connectionUrl = "jdbc:mysql://0.0.0.0:3306/jdbcDemo";
        String user = "root";
        String pw = "DieBohne";
        try(Connection con = DriverManager.getConnection(connectionUrl,user, pw)){
            System.out.println("Verbindung zu DB hergestellt");
            PreparedStatement preparedStatement = con.prepareStatement(sqlSelectAllPersonens);
            ResultSet rs =  preparedStatement.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String ort = rs.getString("ort");
                String strasse = rs.getString("strasse");
                int plz = rs.getInt("plz");
                System.out.println("Student aus der DB: mit der ID " + id + " ort: "+  ort + " strasse: " + strasse + " postleitzahl: " + plz);
            }
        }
        catch (Exception e){
            System.out.println("Verbindung konnte nicht hergestellt werden: " + e);
        }
    }
}
