#Dokumentation zu JDBC Intro
<h3> Einrichtung Datenbankserver </h3>
Dazu wurde XAMPP Installiert sowie in der Entwicklungsumgebung
ein Projekt mit Maven aufgesetzt. 

<h3> Datenbankverbindung herstellen </h3>
In diesem Schritt wurde die Datenbank aufgestzt und eine 
Tabelle eingerichtet. Dabei wurde die Tabelle Students erstellt, 
in welcher 2 Reihen eingefuegt wurden.

Meine Datenbank läuft in einem Docker Container. 

Das Insert Statement lautet folgend: <br>
`INSERT INTO student VALUES (NULL, 'Sojer Florian', 'flsojer@tsn.at'),
(NULL, 'Gustav Klient', 'Gusti@Klient.at');`

Zuletzt wurden ebenfalls, auf der Datenbank, select Statements
getestet. 

### Datenbankverbindung herstellten Teil 2 ###
Die Verbindung zur Datenbank wurde über den Driver-Manager 
aufgebaut. Dabei wurden die Einlog-Parameter wie Username und 
Passwort übergeben und die verbindung getestet. 
Diese verbindung wurde alleine in der funktion: <br>
`selectAllDemo()` getestet. 

### Daten aus der Datenbank abfragen ###
Als erstes wurde in der Funktion `selectAllDemo()`die vorbereitete Datenbankverbindung
verwendet und darin ein Try Catch Block eingefuegt. 

Dabei bereitet man am besten ein SQL Statement vor, in diesem
Falle ist es ein Select all Statement welches Folgend lautet: <br>
`String sqlSelectAllPersonens = "SELECT * FROM student;";`

Um ein SQL Statement auszuführen wird die Funktion `PreparedStatemnt` verwendet. Dabei kann
über die Connection das Statement Vorbereitet werden. 
In einem eigenen try Catch Block, wird dann das Statement ausgeführt. 

Die ausgewählten Daten werden in einem ResultSet gespeichert und darüber können
die Daten über deren Index oder über next abgefragt werden. Bei .next wird lediglich ein boolscher Wert zurückgelierft und
somit kann die While Schleife ablaufen. 

### Daten in eine Datenbank einfügen ### 
Dabei wird wieder das gleiche Schema ausgewählt, wie bei einer Datenbankfabfrage. 
Statt einer Abfrage wird ein Insert-Statement ausgewählt. 

Der unterschied besteht, dass das SQLStatement keine Resultstatements zurückliefert,
lediglich einen Wert, wie viele Reihen davon betroffen waren oder bei einem Fehler ein Exception schmeisst. 

Das PreparedStatement zum einfügen der Daten wird dabei in einem eigenen Try Catch Blcok ausgeführt um 
bei einem Fehler, exact diesen auszuführen und keine Allgemeine Fehlermeldung hervorkommt. 
Um sich ein Beispiel zur Insert-Funktion anzusehen, wird auf die Funktion ÌnsertDemo()`verwiesen. 

### Daten in einer Datenbank Updaten ### 
Im Prinzip funktioniert des gleich als wie man Daten in die Datenbank einügt. 
Dabei wird wieder über das PreparedStatement das executeUpdate ausgeführt. 

### Daten einer Datenbank eintfernen ### 
Der aufbau bleibt dabei wie in jeder Funktion gleich. Lediglich ändert sich das SQL Statement. 
In der hälfte des Videos wurden dann noch die Parameter für die einzelnen Funktionen in der Main erstellt und können so auch Übergeben werden. 
Dabei wird anstatt den festgelegten Strings, die übergebenend parameter eingefügt. 

### Daten einfügen und Abfragen ### 
Das Programm wurde mit einfachen SQL Statements erweitert. 


## JDBC Intro Teil 2 ## 
###Erweiterung der Datenbank ###
Die Datenbank wurde um die Tabelle Adresse erweitert.
Dabei wird sich auf die Adresse jedes Schülers bezogen.
Beinhaltet sind Id (des schülers) , Postleitzahl, Wohnort und Strasse. 

### Erweiterung des Programms ### 
Das Programm wurde gleich wie oben gehandelt und es wurden lediglich die Parameter in den
SQL Statements geändert. 




